<?php

/**
 * @file
 * Includes theme functions for viewfield module.
 */

/**
 * Return HTML for a view in a field.
 */
function theme_viewfield_formatter_default($variables) {
  $element = $variables['element'];
  $view_preview = $element['#view']->preview($element['#view_display'], $element['#view_arguments']);

  return drupal_render($view_preview);
}
