<?php

/**
 * @file
 * Contains \Drupal\viewfield\Plugin\field\formatter\ViewfieldFormatter.
 */

namespace Drupal\viewfield\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\views\Views;

/**
 * Plugin implementation of the 'viewfield' formatter.
 *
 * @FieldFormatter(
 *   id = "viewfield",
 *   label = @Translation("Viewfield"),
 *   field_types = {
 *     "viewfield"
 *   }
 * )
 */
class ViewfieldFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $element = array();
    $entity = $items->getEntity();

    foreach ($items as $delta => $item) {
      // @todo Store name and display separately.
      list($view_name, $view_display) = explode(':', $item->vname, 2);
      $view = Views::getView($view_name);

      $element[$delta] = array(
        '#type' => 'viewfield',
        '#access' => $view && $view->access($view_display),
        '#view' => $view,
        '#view_name' => $view_name,
        '#view_display' => $view_display,
        '#view_arguments' => $item->vargs,
        '#entity_type' => $entity->getEntityType()->id(),
        '#entity_id' => $entity->id(),
        '#entity' => $entity,
      );
    }

    return $element;
  }
}
