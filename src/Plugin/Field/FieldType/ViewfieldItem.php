<?php

/**
 * @file
 * Contains \Drupal\viewfield\Plugin\Field\FieldType\ViewfieldItem.
 */

namespace Drupal\viewfield\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'viewfield' field type.
 *
 * @FieldType(
 *   id = "viewfield",
 *   label = @Translation("Viewfield"),
 *   description = @Translation("Displays a selected view in a node."),
 *   default_widget = "viewfield",
 *   default_formatter = "viewfield"
 * )
 */

class ViewfieldItem extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'vname' => array(
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ),
        'vargs' => array(
          'type' => 'varchar',
          'length' => 128,
          'not null' => FALSE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['vname'] = DataDefinition::create('string')
      ->setLabel(t('View and display name'));
    $properties['vargs'] = DataDefinition::create('string')
      ->setLabel(t('View arguments'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $view = $this->get('vname')->getValue();

    return $view === NULL || $view === '';
  }
}
