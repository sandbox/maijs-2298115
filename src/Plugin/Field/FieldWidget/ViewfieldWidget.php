<?php

/**
 * @file
 * Contains \Drupal\viewfield\Plugin\Field\FieldWidget\ViewfieldWidget.
 */

namespace Drupal\viewfield\Plugin\Field\FieldWidget;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\views\Views;

/**
 * Plugin implementation of the 'viewfield' widget.
 *
 * @FieldWidget(
 *   id = "viewfield",
 *   label = @Translation("Viewfield"),
 *   field_types = {
 *     "viewfield"
 *   }
 * )
 */
class ViewfieldWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, array &$form_state) {
    // If cardinality is 1, wrap the field form elements
    // in "details" form element.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element = array(
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t('Viewfield'),
      ) + $element;
    }

    // View name and display name form element.
    $element['vname'] = array(
      '#type' => 'select',
      '#title' => $this->t('View'),
      '#default_value' => isset($items[$delta]->vname) ? $items[$delta]->vname : NULL,
      '#options' => array('' => $this->t('- Select -')) + Views::getViewsAsOptions(FALSE, 'enabled', NULL, TRUE, TRUE),
    );

    // View arguments name form element.
    $element['vargs'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Arguments'),
      '#default_value' => isset($items[$delta]->vargs) ? $items[$delta]->vargs : NULL,
      '#description' => $this->t('A comma separated list of arguments to pass to the selected view. '),
    );

    return $element;
  }
}
